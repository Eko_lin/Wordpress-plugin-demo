<?php
/* 
Plugin Name: Eko test
Plugin URI:WWW.ekowp.com
Description:eko test plugin
Version:1.0
Author:Eko
License: A "Slug" license name e.g. GPL2
*/ 

//定义plugin version
define('PLU_VERSION','1.0');
//定义当前plugin路径
define('PLU_URL',plugin_dir_url(__FILE__));
//定义当前plugin文件夹
define('PLU_DIR',plugin_dir_path(__FILE__));
//引入当前plugin class file
require_once(PLU_DIR.'class.eko-test.php');

//激活plugin之后的基本属性
register_activation_hook(__FILE__,array('Ekotest','prefix_activation'));
register_activation_hook(__FILE__,array('Ekotest','prefix_deactivation'));

//初始化信息
//add_action('wp',array('Ekotest','wp_init'));
add_action('init',array('Ekotest','init'));
//加载后台页面
if(is_admin()){
   require_once(PLU_DIR.'class.eko-test-admin.php');
   add_action('init',array('EkotestAdmin','init'));   
}