<?php
  if(!class_exists(EkotestAdmin)){
     class EkotestAdmin{
      public static function init(){
      	//添加wporg_options_page 方法在admin_menu这个动作下、
     	add_action('admin_menu', array(get_called_class(),'wporg_options_page'));
     	//添加wporg_options_page 方法在admin_init这个动作下、
     	add_action('admin_init',array(get_called_class(),'register_settings'));
       }
      //注册form表单提交的数据，WordPress 提交表单是用组来做单位
      public static function register_settings(){
        register_setting('my_options_group','testname');
        register_setting('my_options_group','testsex');
      }

      public static function add_page(){
      	// check user capabilities
	    if (!current_user_can('manage_options')) {
	        return;
	    }
	    ?>
	     <div class="wrap">
	        <h1><?= esc_html(get_admin_page_title()); ?></h1>
		        <form action="options.php" method="post">
		          <?php settings_fields('my_options_group'); ?>
		          添加字符: <input type="text"  name="testname" value="<?php echo esc_attr(get_option('testname')) ?>" size="40" /><br/>
				  
				 
		          <?php submit_button() ?>
		        </form>
    	</div>
	    <?php
      }
       
	 public static function wporg_options_page(){
		    add_submenu_page(
		        'options-general.php',
		        __('Eko test Settings'),
		        __('Eko test '),
		        'manage_options',
		        'ekotest',
		        array(get_called_class(), 'add_page')
		    );
		    
		}	
     }
  }